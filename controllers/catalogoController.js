const { getDBdeCatalogo, guardarCatalogoDB } = require("../helpers/guardarCatalogo");
const Catalogo = require("../models/catalogo");
const Catalogos = require("../models/catalogos");

const { response } = 'express';

const catalogos = new Catalogos();

const catalogoGet = (req, res = response) => {
  const catalogosDB =  getDBdeCatalogo();
  if (catalogosDB) {
    catalogos.cargarCatalogosEnArray(catalogosDB);
  }

  res.json({
    msg: 'get API - Controlador',
    catalogosDB
  });
};

const catalogoPut = (req, res = response) => {
  //ACTUALIZAR EL CATALOGO
  const catalogosDB =  getDBdeCatalogo();
  if (catalogosDB) {
    catalogos.cargarCatalogosEnArray(catalogosDB);
  }
 const {id} = req.params;
 if (id) {
   catalogos.borrarProducto(id);
   const { nombre, categoria, numero_serie, fecha_caducidad} = req.body;
  const producto = new Catalogo(nombre, categoria, numero_serie, fecha_caducidad);
   
   producto.setID(id);
   catalogos.crearProducto(producto);
   guardarCatalogoDB(catalogos.listadoArr);
 }
 res.json({
   msg: 'put API - Controlador'
 });
};

const catalogoPost = (req, res = response) => {

  const { nombre, categoria, numero_serie, fecha_caducidad} = req.body;
  const producto = new Catalogo(nombre, categoria, numero_serie, fecha_caducidad);

  let catalogosDB =  getDBdeCatalogo();
  if (catalogosDB) {
    catalogos.cargarCatalogosEnArray(catalogosDB);
  }


  catalogos.crearProducto(producto);
  // console.log(listado);
  guardarCatalogoDB(catalogos.listadoArr);
  catalogosDB =  getDBdeCatalogo();
  res.json({
    msg: 'post API - Controlador',
    catalogosDB
  });
  res.json({
    msg: 'post API - Controlador',
    listado
  });
};

const catalogoDelete = (req, res = response) => {
  const catalogosDB =  getDBdeCatalogo();
  if (catalogosDB) {
    catalogos.cargarCatalogosEnArray(catalogosDB);
  }
 const {id} = req.params;
 // console.log(id);
 if (id) {
   catalogos.borrarProducto(id);
   guardarCatalogoDB(catalogos.listadoArr);
    //console.log(catalogos._listado);
 }


  res.json({
    msg: 'delete API - Controlador'
  });
};

module.exports = {
  catalogoGet,
  catalogoPost,
  catalogoPut,
  catalogoDelete
 
}