const { getDB, guardarDB } = require("../helpers/guardarArchivo");
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const { response } = 'express';

const personas = new Personas();

const personaGet = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }

  res.json({
    msg: 'get API - Controlador',
    personasDB
  });
};

const personaPut = (req, res = response) => {
//actualiza lista
const personasDB =  getDB();
if (personasDB) {
  personas.cargarPersonasEnArray(personasDB);
}
const {id} = req.params;
if (id) {
 personas.borrarPersona(id);
 const { nombres, apellidos, ci } = req.body;
 const persona = new Persona(nombres, apellidos, ci);
 persona.setID(id);
 personas.crearPersona(persona);
 guardarDB(personas.listadoArr);
}
  res.json({
    msg: 'put API - Controlador'
  });
};

const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci} = req.body;
  const persona = new Persona(nombres, apellidos, ci);

  let personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }

  personas.crearPersona(persona);
  // console.log(listado);
  guardarDB(personas.listadoArr);
  personasDB =  getDB();
  res.json({
    msg: 'post API - Controlador',
    personasDB
  });
  res.json({
    msg: 'post API - Controlador',
    listado
  });
};


const personaDelete = (req, res = response) => {
  const personasDB =  getDB();
  if (personasDB) {
    personas.cargarPersonasEnArray(personasDB);
  }
 const {id} = req.params;
 // console.log(id);
 if (id) {
   personas.borrarPersona(id);
   guardarDB(personas.listadoArr);
    //console.log(catalogos._listado);
 }


  res.json({
    msg: 'delete API - Controlador'
  });
};

module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete
}