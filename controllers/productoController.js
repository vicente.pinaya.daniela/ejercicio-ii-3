const { getDBdeProducto, guardarProductoDB } = require("../helpers/guardarProductos");
const Producto = require("../models/producto");
const Productos = require("../models/productos");

const { response } = 'express';

const productos = new Productos();

const productoGet = (req, res = response) => {
  const productosDB =  getDBdeProducto();
  if (productosDB) {
    productos.cargarProductosEnArray(productosDB);
  }

  res.json({
    msg: 'get API - Controlador',
    productosDB
  });
};

const productoPut = (req, res = response) => {
  //ACTUALIZAR EL producto
  const productosDB = getDBdeProducto();
  if (productosDB) {
    productos.cargarProductosEnArray(productosDB);
  }
 const {id} = req.params;
 if (id) {
   productos.borrarProductoOficial(id);
   const { nombre, categoria, numero_serie, fecha_caducidad} = req.body;
  const producto = new Producto(nombre, categoria, numero_serie, fecha_caducidad);
   
   producto.setID(id);
   productos.crearProductoOficial(producto);
   guardarProductoDB(productos.listadoArr);
 }
 res.json({
   msg: 'put API - Controlador'
 });
};

const productoPost = (req, res = response) => {

  const { nombre, categoria, numero_serie, fecha_caducidad} = req.body;
  const producto = new Producto(nombre, categoria, numero_serie, fecha_caducidad);

  let productosDB =  getDBdeProducto();
  if (productosDB) {
    productos.cargarProductosEnArray(productosDB);
  }


  productos.crearProductoOficial(producto);
  // console.log(listado);
  guardarProductoDB(productos.listadoArr);
  productosDB = getDBdeProducto();
  res.json({
    msg: 'post API - Controlador',
    productosDB
  });
 

  
  res.json({
    msg: 'post API - Controlador',
    listado
  });
};

const productoDelete = (req, res = response) => {
  const productosDB = getDBdeProducto();
  if (productosDB) {
    productos.cargarProductosEnArray(productosDB);
  }
 const {id} = req.params;
 // console.log(id);
 if (id) {
   productos.borrarProductoOficial(id);
   guardarProductoDB(productos.listadoArr);
    //console.log(productos._listado);
 }


  res.json({
    msg: 'delete API - Controlador'
  });
};

module.exports = {
  productoGet,
  productoPost,
  productoPut,
  productoDelete
 
}