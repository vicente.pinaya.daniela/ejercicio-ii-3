const fs =require('fs');

const archivoCatalogo ='./db/catalogo.json';

const guardarCatalogoDB = (data) => {
  // console.log(data);
  fs.writeFileSync(archivoCatalogo ,JSON.stringify(data));
}

const getDBdeCatalogo = () =>{
  if (!fs.existsSync(archivoCatalogo)) {
    return null;
  }

  //leer el archivo :
  fs
  const info = fs.readFileSync(archivoCatalogo, {encoding: 'utf-8'});

  const data  = JSON.parse(info);
  // console.log(data);
  return data;
}

module.exports ={
  guardarCatalogoDB,
  getDBdeCatalogo
}