const { v4: uuidv4 } = require('uuid');

class Catalogo {
  
  id = '';
  nombre = '';
  categoria = '';
  numero_serie = 0;
  fecha_caducidad = '';

  constructor(nombre, categoria, numero_serie, fecha_caducidad) {
    this.id = uuidv4();
    this.nombre = nombre;
    this.categoria = categoria;
    this.numero_serie = numero_serie;
    this.fecha_caducidad = fecha_caducidad;
  
    
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Catalogo;


