class Catalogos {

  constructor() {
    this._listado = [];
  }

  get listadoArr(){
    const listado  =[];
    //Me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key =>{
      //console.log(key);
      const catalogo = this._listado[key];
      //console.log(persona);
      listado.push(catalogo);
    })
 
    return listado;
  }
 
  crearProducto(catalogo = {}) {
    this._listado[catalogo.id] = catalogo;

    return this._listado;
  }

  
  cargarCatalogosEnArray(catalogos=[]){
    //console.log(catalogos);
    catalogos.forEach(catalogo =>{
      this._listado[catalogo.id]=catalogo;
    })
 
    //console.log(this._listado);
   }


   borrarProducto(id ){
    console.log(this._listado);
    delete this._listado[id];
    console.log(this._listado);

  }





}

module.exports = Catalogos;