const { v4: uuidv4 } = require('uuid');

class Persona {
  
  id = '';
  nombres = '';
  apellidos = '';
  ci = 0;



  constructor(nombres, apellidos, ci) {
    this.id = uuidv4();
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.ci = ci;
    
  }

  setID(idx){
    this.id =idx;
  }
}

module.exports = Persona;