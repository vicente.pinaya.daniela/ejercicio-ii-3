class Productos {

  constructor() {
    this._listado = [];
  }


  get listadoArr(){
    const listado  =[];
    //Me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key =>{
      //console.log(key);
      const producto = this._listado[key];
      //console.log(persona);
      listado.push(producto);
    })
 
    return listado;
  }
 

  crearProductoOficial(producto = {}) {
    this._listado[producto.id] = producto;
   
    return this._listado;
  }

  cargarProductosEnArray(productos=[]){
    //console.log(personas);
    productos.forEach(producto =>{
      this._listado[producto.id]=producto;
    })
 
    //console.log(this._listado);
   }



   borrarProductoOficial(id ){
    console.log(this._listado);
    delete this._listado[id];
    console.log(this._listado);

  }



}

module.exports = Productos;