const { Router } = require('express');
const {
catalogoGet,
catalogoPut,
catalogoPost,
catalogoDelete
} = require('../controllers/catalogoController');

const router = Router();

router.get('/', catalogoGet);

router.put('/:id', catalogoPut);

router.post('/', catalogoPost);

router.delete('/:id', catalogoDelete);

module.exports = router;